package ru.codeinside.lessons.javacore.inheritance;

import ru.codeinside.lessons.javacore.inheritance.enums.VehicleColor;

import java.util.Random;

public abstract class Vehicle {

    private final String vin;
    private int yearOfProduction;
    private VehicleColor color;

    public abstract String getVehicleInfo();

    public String getVin() {
        return vin;
    }

    public Vehicle(int yearOfProduction, VehicleColor color) {
        this.yearOfProduction = yearOfProduction;
        this.color = color;
        this.vin = generateVin();
    }

    public Vehicle(int yearOfProduction, String color) {
        this.yearOfProduction = yearOfProduction;
        this.color = VehicleColor.getColorByName(color);
        this.vin = generateVin();
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public VehicleColor getColor() {
        return color;
    }

    public void setColor(VehicleColor color) {
        this.color = color;
    }

    /**
     * Метод генерирует псевдо-случайный идентификационный номер транспортного средства.
     * Данный номер используется в целях обучения и не соответствует ISO 3779-1983 и ISO 3780.
     * Переделывать или вносить изменения в этот метод не требуется.
     *
     * @return псевдо-уникальный VIN
     */
    private static String generateVin() {
        var source = "ABCDEFGHJKLMNPQRSTUVWXYZ1234567890";
        var sb = new StringBuilder();
        int i = 0;
        var random = new Random();
        while (i < 17) {
            sb.append(source.charAt(random.nextInt(source.length())));
            i++;
        }
        return sb.toString();
    }
}

package ru.codeinside.lessons.javacore.implementation;

public class Freelancer implements Service {

    private String nameFreelancer;
    private String nickName;
    private String eWallet;

    Freelancer(String nameFreelancer, String nickName, String eWallet) {
        this.nameFreelancer = nameFreelancer;
        this.nickName = nickName;
        this.eWallet = eWallet;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getWallet() {
        return eWallet;
    }

    public void setWallet(String eWallet) {
        this.eWallet = eWallet;
    }

    public void setNameFreelancer(String nameFreelancer) {
        this.nameFreelancer = nameFreelancer;
    }

    @Override
    public String makeService() {
        return nameFreelancer;
    }

    @Override
    public String getName() {
        return nickName;
    }

}

package ru.codeinside.lessons.javacore.implementation;

public class Company implements Service {

    private String nameCompany;
    private String nameBank;
    private String numberINN;
    private String bankAccount;

    Company(String nameCompany, String nameBank, String numberINN, String bankAccount) {
        this.nameCompany = nameCompany;
        this.nameBank = nameBank;
        this.numberINN = numberINN;
        this.bankAccount = bankAccount;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getNameBank() {
        return nameBank;
    }

    public void setNameBank(String nameBank) {
        this.nameBank = nameBank;
    }

    public String getNumberINN() {
        return numberINN;
    }

    public void setNumberINN(String numberINN) {
        this.numberINN = numberINN;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public String makeService() {
        return "Company of Heroes";
    }

    @Override
    public String getName() {
        return String.format("%s, %s", nameCompany, numberINN);
    }

    @Override
    public String getShortName() {
        return nameCompany;
    }

}
